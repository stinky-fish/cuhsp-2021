(Special thanks to Phillip McKnight)

## Virtual Machine

![](/Images/lubuntu.png)
1. Arduino IDE - This is where you will create and upload your scripts to the Badge
2. File explorer - You can manage your files here
3. Web Browser - This is the defalut web browser used in lubuntu
4. Terminal - The application use to issues commands
5. Power button - Use this to shut down/restart/sleep


## Creds
User : os21  
Password : summit21

## Instuctions
* Download and install the latest version of [VMware Player](https://my.vmware.com/web/vmware/downloads/details?downloadGroup=PLAYER-1510&productId=800) (it's free for Windows and Linux)
    * [Virtual Box](https://www.virtualbox.org) should be able to convert and run the image as well if you have a Mac.
* Download and unzip the [virtual machine image](https://cisco.box.com/v/OS-2020-Badge-VM) it has Arduino already properly configured for working with the badge.
* Select "Open VM" and point VMware Player to where you unziped the VM
* Login using the creds provided above.

## Troubleshooting
* Sometimes when suspending the VM without shutting it down will cause it to lose internet connectivity.  If this happens open the terminal and type `sudo service network-manager restart`. Enter the password above when prompted and this should bring you back on line.  You can test the connection by using `ping -c 3 www.google.com`. If a ping is returned you are good to go.
