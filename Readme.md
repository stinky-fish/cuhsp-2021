## Welcome!!

This repository contains information and tutorials to guide you through the basics of programming the 2020-2021 Offensive Summit Badge.  

Here are the [package contents](/Package%20Contents/readme.md) that you should have received.  

Here you can find the the [schematic](/Schematic/OS-2020-Badge-REV0-Schematic.PDF).  

Here you can find 3D renderings of the [top](Images/OS-2020-Badge-REV0-3D-Render-Front.png) and [bottom](/Images/OS-2020-Badge-REV0-3D-Render-Back.png) of the printed circuit board.  

Here you can find 2D images of the [top](/Images/OS-2020-Badge-REV0-2D-Render-Front.png) and [bottom](/Images/OS-2020-Badge-REV0-2D-Render-Back.png) routing.   

Here you can find the [PCB fabrication and assembly data](/Production%20Data) in case you want to have your own PCBs made.  

Here you can find a [video guide to assembling and soldering](/Tutorials/Badge-Soldering.md) the badge.  

Start off by setting up a programming environment to compile and upload code.  If you are using a Raspberry Pie (Debian Linux) system complete the [Raspberry Pi](Getting-Started-Raspberry-Pi.md) and [Arduino](Getting-Started-Arduino.md) setup instructions.  Windows users can install Arduino from the Windows store and then complete the [Arduino](Getting-Started-Arduino.md) setup instructions.  Alternative to installing Arduino natively you can download and use a pre-configured [Virtual Machine](/VM/readme.md) environment to compile and upload code.  

Here are [instructions to setup Webex and join the CUHSP 2024 Webex Team Space](Getting-Started-Webex.md).  We will use Webex to communicate, share code, screen shots, etc.    

Once your environment is setup you can proceed to the tutorials, starting with LED Blink:  

[LED Blink Tutorial](/Tutorials/LED-Blink-Tutorial.md)  

[Screen Text Tutorial](/Tutorials/Screen-Text-Tutorial.md)  

[Screen Image Tutorial](/Tutorials/Screen-Image-Tutorial.md)  

[Touch Button Tutorial](Tutorials/Touch-Button-Tutorial.md)  

[Web Server Tutorial](/Tutorials/Web-Server-Tutorial.md) 

[GIF Animation Tutorial](/Tutorials/GIF-Tutorial.md)   

[Buzzer Speaker Tutorial](/Tutorials/Buzzer-tutorial.md)  

[ // ]: < [LED Ring Tutorial](/Tutorials/LED-Ring-Tutorial.md) >  

[Joystick Tutorial](/Tutorials/Joystick-Tutorial.md)  

[Battery Tutorial](/Tutorials/Battery-Tutorial.md)

[ // ]: < [Oscilloscope Tutorial](/Tutorials/simple_oscilloscope.md) >




