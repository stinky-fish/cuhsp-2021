# Getting started Raspberry Pi (Debian)

Before installing Arduino and writing code we need to make sure our environment is up to date.  

Raspberry Pies come with the Raspian operating system, which is based on Debian Linux.  

You can use other environments (Windows, Mac, Other Linux Distros), but the instructions throughout this repo assume you are working in a Raspberry Pi 5 with Raspian OS installed.   

Here are [official instructions](https://www.raspberrypi.com/documentation/computers/os.html) for updating and upgrading your Raspberry Pi; you can follow these or you can just continue below.  

## APT and which
The Raspberry Pi operating system (Raspian OS) is based on Debian Linux.  On debian based versions of Linux we can use the Advanced Packaging Tool (APT) to install tooling.  

You can test if you already have apt (or any other program) with the which command.  Open a terminal window and enter this command:

`which apt`  

If APT exists on your Raspberry Pi OS you will see something like this returned:
 ```   
user@hostname:~$ which apt             
/usr/local/bin/apt         
```     
     
If the program does not exist you will see nothing returned:
```
user@hostname:~$ which calculator    
user@hostmame:~$   
```
Above we see that nothing is returned so it means there is not a program or application called calculator installed on this system.  The 'which' command is a great way to figure out if something exists on a Linux system.  

## Home directory and Print Working Directory 

You can always navigate to your home directory with this command:  

`cd ~`

This is quick way to jump to your home directory without having to type the full path. 

You can always show the full path by using the 'print working directory' command.

Run the pwd command in your terminal window:

`pwd`

You should see something like this returned (but specific to your username and hostname):

user@hostname:~$ pwd  
/home/user  


## sudo, APT update and APT upgrade  

Next we will update our package lists to make sure we install the very latest Raspberry Pi environment.  The APT program often needs write access to disk areas which are only accessible to by adminstrator accounts (aka the root account).  To give the installer elevated privileges to write anywhere it needs we will use the sudo command.  The sudo command allows for users to run programs with the security privileges of another user, by default the superuser.  The sudo command originally stands for "supeuserdo"    

Before doing the actual install command we should tell APT to download the latest package lists from the currently selected software repositories with a sudo command:  

`sudo apt update`  

Since we are invoking elevated privileges it will ask you for your password.  The output should show apt hitting a few repositories and updating the package list (update for raspian specific output):    
```
user@hostname:~$ sudo apt update  
[sudo] password for user:             
Hit:1 http://security.ubuntu.com/ubuntu bionic-security InRelease  
Hit:2 http://archive.ubuntu.com/ubuntu bionic InRelease  
Hit:3 http://archive.canonical.com/ubuntu bionic InRelease  
Hit:4 http://archive.ubuntu.com/ubuntu bionic-updates InRelease                  
Ign:5 http://packages.linuxmint.com tricia InRelease                             
Hit:6 http://packages.linuxmint.com tricia Release  
Hit:7 http://archive.ubuntu.com/ubuntu bionic-backports InRelease  
Reading package lists... Done                        
Building dependency tree         
Reading state information... Done  
33 packages can be upgraded. Run 'apt list --upgradable' to see them.  
user@hostname:~$   
```

Once the package lists have been updated we should install available upgrades of all packages currently installed on the system with the upgrade command:

`sudo apt upgrade`  

This command may take a while depending on how out-dated your system is.  You will see APT updating any outdated packages.  Grab some popcorn.  Your ouput should be similar to this (update for raspberry Pi):
```
user@host:~$ sudo apt upgrade  
[sudo] password for user:           
Reading package lists... Done  
Building dependency tree         
Reading state information... Done  
Calculating upgrade... Done  
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.  
user@hostname:~$   
```








