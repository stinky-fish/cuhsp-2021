import os
import pdb
import sys
import binascii
import numpy as np
from PIL import Image

def extractFrames(inGif, outFolder):
    frame = Image.open(inGif)
    nframes = 0
    while frame:
        frame.save( '%s/%s-%s.gif' % (outFolder, os.path.basename(inGif), nframes ) , 'GIF')
        nframes += 1
        try:
            frame.seek( nframes )
        except EOFError:
            break;
    return nframes
def convertFrames(GIF_name) :
   with Image.open(GIF_name) as img:
        resized = img.resize((128,128))
        x = 0;
        y = 0;
        for y in range(img.n_frames):
            img.seek(y)
            for row in np.array(resized):
                for pixel in row:
                    # INDEX to RGB
                    Rvalue = resized.palette.palette[pixel*3]
                    Gvalue = resized.palette.palette[pixel*3+1]
                    Bvalue = resized.palette.palette[pixel*3+2]
                    # RGB to 565
                    pixelvalue = ((ord(Rvalue) & 0xf8) << 8) | ((ord(Gvalue) & 0xfc) << 3) | (ord(Bvalue) >> 3)


                    bytes = hex(pixelvalue)[2:]
                    first_byte = "";
                    second_byte = "";
                    for i in range(len(bytes)):
                    
                        if (len(bytes) == 3): 
                            if (i < 1):
                                first_byte += "0" + bytes[i];
                            else:
                                second_byte += bytes[i];
                    
    
                        elif (len(bytes) == 2):

                            second_byte += bytes[i];
                            first_byte += "0";

                
                        elif (len(bytes) == 1):
                        
                            first_byte = "00";
                            second_byte = "0" + bytes[i];


                        else:
                       
                            if (i < 2):
                                first_byte += bytes[i];
                            else:
                                second_byte += bytes[i];
                
                
                    hex_1 = binascii.unhexlify(first_byte)
                    hex_2 = binascii.unhexlify(second_byte)
                    g.write(hex_1 + hex_2)
                    x = x + 1

outputFolder = sys.argv[2]
GIF_file = sys.argv[1]
output_file = sys.argv[3]
output_binary = sys.argv[4]
output_path = outputFolder + '/' + output_file
output_path2 = outputFolder + '/' + output_binary
if not os.path.exists(outputFolder):
    os.mkdir(outputFolder)
f = open(output_path,'w')
g = open(output_path2, 'w')
frames = extractFrames(GIF_file, outputFolder)
f.write("int num_frames = " + str(frames) + ";" + "\n")

# for loop that goes up to nframes calling the convertFrames and increases the gif
# file number eachtime up to the nframes value and saves into one big output file
z = 0 

for z in range(frames):
     name = ('%s/%s-%s.gif' % (outputFolder, os.path.basename(GIF_file), z))
     convertFrames(name)# gif file name in output Folder  
   






