Here are instructions to setup Webex on in your Raspberry Pi environment. 

You can [use it directly in a web browser](https://web.webex.com/sign-in) or you can [download and install the Webex application](https://www.webex.com/downloads.html) in your environment. The Raspberry Pi environment (Raspian OS) is based on Debian Linux; make sure to pick Other Operating systems --> Download for Linux (.deb) if you are installing the Webex Application to your Raspberry Pi environment.

To install after downloading use the terminal, first change to the directory where it was downloaded:  
`cd ~/Downloads`

Then use the apt package manager to install it:  
`sudo apt-get install ./Webex.deb`

Here is the link to our [2024 CUHSP Webex Teams Space](webexteams://im?space=8ffbad30-2fd5-11ef-9b17-f1f4ccaca62f).  Once you are logged into the broswer application or the desktop application you can click the link to bring up the space.  If you do not have a Webex account you should see an invite to join Webex in your personal email where you received communications about this course. 