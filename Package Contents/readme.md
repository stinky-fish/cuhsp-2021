### Package Contents

In your package of goodies you can find:

1 x [OS 2020-2021 Badge](/Images/OS-2020-Badge-REV0-3D-Render-Front.png)  
1 x [18650 Lithium Ion Battery](/Images/18650-battery.jpg)  
1 x [1.44" 128x128 TFT_LCD Screen](/Images/128x128-SPI-TFT-LCD.jpg)  
1 x [Adafruit Micro Lipo module](/Images/Adafruit-Micro-Lipo-USB-C.jpg)  
1 x [USB-C cable](/Images/Red-USB-C-Cable.jpg)  
1 x [USB to TTL serial Dongle](/Images/USB-2-Serial-Dongle.jpg)  
1 x [37-in-1 Sensor Kit](https://www.aliexpress.com/item/4000151281919.html)  
1 x [Breadboard and Component Kit](https://www.aliexpress.com/item/32884345397.html)  

Small Bag "A"  
2 x [long standoffs](/Images/PCB-Standoff.jpg) for screen mounting - ([93330A251](https://mcmaster.com/93330A251))  
12 x [tiny phillips screws](/Images/Tiny-phillips-screw.jpg) for mounting screen (4 each) and micro lipo module (8 each) - ([90272A025](https://mcmaster.com/90272A025))   
4 x [short standoffs](/Images/PCB-Standoff.jpg) for Micro Lipo module mounting - ([93330A211](https://mcmaster.com/93330A211))  

Small Bag "B"  
4 x [4-40 large standoff](/Images/Large-Standoff.jpg) for pcb corner mounting - ([93330A437](https://mcmaster.com/93330A437))  
4 x [4-40 pan screw](/Images/pan-screw.jpg) for large standoff - ([91772A105](https://mcmaster.com/91772A105))   

Small Bag "C"  
2 x [4-40 hex nuts](/Images/4-40-hex-nut.jpg) for battery holder - ([91771A106](https://mcmaster.com/91771A106))  
2 x [4-40 flat phillips screw](/Images/flat-phillips-screw.jpg) for battery holder - ([90730A005](https://mcmaster.com/90730A005))   



What you will need to complete all [projects](/Tutorials) listed in this repository:   
[Patience](https://media.giphy.com/media/j5oQQW3YArV0cVZowy/giphy.gif)  
[Willpower](https://media.giphy.com/media/tdWlfngG47WMCUXjZu/giphy.gif)  
[Time](https://media.giphy.com/media/WUUt2ujWlssvUenOuf/giphy.gif)  
Things that may be helpful, depending on the project you want to do:  
[Solder Kit](https://www.amazon.com/Electronics-Soldering-Portable-Auto-sleep-Thermostatic/dp/B08GLB15K7)   
[Charcoal Filter](https://www.amazon.com/Kulannder-Absorber-Remover-Extrator-Prevention/dp/B08F5DN2B8)  
[Flux Capacitor](https://www.oreillyauto.com/flux-500.html)  
