## PCB Production Data

Here you an find the production data package that was sent to the contract manufacturer to create and populate the 2020-2021 Offensive Summit Badge PCB.  

The [OS-2021-Badge-REV0.zip](/Production%20Data/OS-2021-Badge-REV0.zip) file contains the gerber and NC Drill data to create the printed circuit board (PCB) as well as the Bill of Materials (BOM) and Assembly (Pick-and-place) data.  
