## Screen Image Tutorial

Make sure you have done the [raspberry pi setup](Getting-Started-Raspberry-Pi.md)

Make sure you have done the  [arduino setup](Getting-Started-Arduino.md)

Make sure you have installed the screen and imported the TFT_eSPI library and modified your User_Setup.h as detailed in the [screen text tutorial](Screen-Text-Tutorial.md)

## Find an Image

Use the world wide web to find a .jpg or .png or .gif image you would like to display on the screen.   

Save it to your Downloads folder and rename it to my_image.jpg (or my_image.png or my_image.gif)  

## Resize the Image

Our screen is 128x128 pixels.  It is unlikely that the image you picked is 128x128 so we will need to resize the image.

Open an terminal and use APT to nstall the GNU impage manipulation program (gimp):

`sudo apt install gimp`

Once it is finished installing you can find gimp under the start menu.  

Run gimp and open my_image.jpg by navigating to File -> Open

Navigate to Image -> Scale Image and set Width = 128 and Height = 128 pixels

Export the new image by navigating to File -> Export As and then export it to your downloads folder as my_image_sized.jpg (or my_image_sized.png or my_image_sized.gif)

## Convert the Image to RGB565

Our display uses RGB565 format.  

We are lucky because some smart people on the internet made a website that can convert .jpg, .png, and .gif images to RGB565 format.

Use the world wide web to navigate to http://www.rinkydinkelectronics.com/t_imageconverter565.php

Upload my_image_sized.png and convert it to .c file format.  Save the returned file (my_image_sized.c) in your downloads folder.  

## Import Image data to Arduino IDE environment

Open a new sketch.  

Navigate to File -> Save As and name it Arduino-Image

Open a terminal use cp command to copy and reaname the converted image file as my_image_sized.h and place it into the new Arduino Project folder:

` cp ~/Downloads/my_image_sized.c ~/Arduino-Sketchbooks/Arduino-Image/my_image_sized.h `

Note that the above step has renamed the file from my_image_sized.c to my_image_sized.h!!

Close Arduino and re-open it. 

Navigate to File -> Open and select the ~/Arduino/Arduino-Image.ino file.

You should now see two files in the Arduino IDE:  Arduino-Image and my_image_sized.h

##  Code 

To use the display we will utilize the custom [TFT_eSPI](https://github.com/Bodmer/TFT_eSPI) library, include these header files above the setup function:  
`#include <TFT_eSPI.h>`   
`#include "my_image_sized.h"`   

Invoke the custom TFT_eSPI library after the include statements:  
`TFT_eSPI tft = TFT_eSPI();`  

Extend the setup function to initalize the screen and show our image:  
```
void setup() {
  // put your setup code here, to run once:
  tft.init();   //initialize the screen
  tft.setSwapBytes(1);  //fix the endianness of the data
  tft.pushImage(0,0,128,128,my_Image_sized);   //draw our custom image

}

```

Compile, upload, and run your code!!  If your image shows up then congratulations, you have successfully converted your image data and loaded it into RAM memory.  
