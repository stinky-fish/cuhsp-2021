## Web Server Tutorial

(special thanks to Paul Dewitt)

Make sure you have done the [raspberry pi setup](Getting-Started-Raspberry-Pi.md) as well as the [arduino setup](Getting-Started-Arduino.md).

(This does not require any soldering.)

The ESP32 chiip on the badge has integrated Wi-Fi.  In this tutorial we will make use of it to serve a Web page on the local network.

## Code

This code comes from the examples for ESP32.

Open it in Arduino with File -> Examples -> WebServer -> HelloServer  or copy the code below:

```
#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <ESPmDNS.h>

const char* ssid = "........";
const char* password = "........";

WebServer server(80);

const int led = 13;

void handleRoot() {
  digitalWrite(led, 1);
  server.send(200, "text/plain", "hello from esp8266!");
  digitalWrite(led, 0);
}

void handleNotFound() {
  digitalWrite(led, 1);
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
  digitalWrite(led, 0);
}

void setup(void) {
  pinMode(led, OUTPUT);
  digitalWrite(led, 0);
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("esp32")) {
    Serial.println("MDNS responder started");
  }

  server.on("/", handleRoot);

  server.on("/inline", []() {
    server.send(200, "text/plain", "this works as well");
  });

  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}

void loop(void) {
  server.handleClient();
}
```

### Code Changes

You will need to make a few changes to make this work:

Change these lines to match your wireless network's SSID and password:
```
const char* ssid = "........";
const char* password = "........";
```

The code will make the LED blink when a page is visitied.  Recall from the LED
tutorial that the badge's LED is on port 22, so change this line:
```
const int led = 13;
```
To:
```
const int led = 22;
```

And if you want to customize the message, replace `hello from esp8266!` on this line in `handleRoot()`: 
```
  server.send(200, "text/plain", "hello from esp8266!");
```

Compile and upload your modified code.

## Run and Test

When you start the server, you'll need to watch the Serial Monitor to learn it's
IP address.  Navigate to Tools -> Serial Monitor.  Make sure the Serial Monitor
is set to 115200 baud rate in the lower right dialog box.

Shortly after a reset it should say that it has connected to the SSID and show
you the IP address.  If it takes a minute with the Serial Monitor just printing
'.' then it may be having troubles making a connection; you can try pressing S9
to reset the badge and try again, otherwise check the SSID and password, move
closer to the wireless router: all the usual WI-FI debugging steps.

Once you have the IP address, put it into a browser of a device on the same
network, and you should see your badge's message.
