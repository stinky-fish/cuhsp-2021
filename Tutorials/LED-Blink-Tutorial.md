## LED Blink Tutorial

Make sure you have done the [raspberry pi setup](Getting-Started-Raspberry-Pi.md) as well as the [arduino setup](Getting-Started-Arduino.md).

Open the [schematic](/Schematic/OS-2020-Badge-REV0-Schematic.PDF) in a separate tab.

In the LED area of the circuit we can see D1 component.  D1 is an "LED" which is short for Light Emitting Diode.

D1 is connected to resistor R2 which has a net name of LED1.     

Zoom into component U2 in the middle of the page.  

Note that LED1 net also connects pin number 36 of U2.  In a "flat schematic" like this one, any wires with the same net label are connected.  

Note that pin label is "IO22" which means input/output port number 22.  

In order to make the LED1 blink we MUST USE THE PORT NUMBER in our code (not the pin number).  

For any signal that we want/can control on the ESP32 processor we must ALWAYS USE PORT NUMBERS in our code (not pin numbers).  

Note that TP22 is also connected to LED1 net.  TP22 means 'test point 22'; we can touch or solder into the TP22 pad on the PCB to access the LED1 net.  


## Code

Open a new sketch. 

Navigate to File -> Save As and name it Arduino-LED-Blink

Add a line to define a constant for referencing our port number:
```
#define LED1 22
```

The setup function gets called each time you press reset or power the board.  Add code to extend the setup function and call [pinmode()](https://www.arduino.cc/reference/en/language/functions/digital-io/pinmode/) to assign port 22 as an output:

```
void setup() {
  // initialize digital pin LED1 as an output
  pinMode(LED1, OUTPUT);

}
```

The loop function runs over and over again forever.  Add code to extend the loop function. Use [digitalWrite()](https://www.arduino.cc/reference/en/language/functions/digital-io/digitalwrite/) to set the LED1 net to [HIGH](https://www.arduino.cc/reference/en/language/variables/constants/constants/) or [LOW](https://www.arduino.cc/reference/en/language/variables/constants/constants/) in order to turn on and off the LED.  Use [delay()](https://www.arduino.cc/reference/en/language/functions/time/delay/) to control the blink rate:

```
void loop() {
 
  digitalWrite(LED1, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                // wait for 1000 milliseconds (1000 milliseconds = 1 second)
  
  digitalWrite(LED1, LOW);    // turn the LED off by making the voltage LOW 
  delay(1000);                // wait for 1000 milliseconds

}
```

## Compile
Compile your code by clicking the check mark button under the menu bar or choose Sketch -> Verify/Compile.  
If all goes well you should see something like this in the output:

```
Done compiling.  
Sketch uses 253477 bytes (19%) of program storage space. Maximum is 1310720 bytes.  
Global variables use 15276 bytes (4%) of dynamic memory, leaving 312404 bytes for local variables. Maximum is 327680 bytes.  
```

Congratulations - you are now ready to upload your code!

## Serial Upload

The OS2020 badge can accept code from a serial interface (P1 header) OR a USB interface (P5 header).  This section details the instructions for serial interface programming.

The ESP32 will enter the serial bootloader when GPIO0 is held low during a reset event.  We can hold GPIO0 low by pressing the S1 button.  We can cause a reset event by pressing the S9 button.  To put the ESP32 into bootloader mode push and hold the S1 button (to assert GPIO0 to low) then push and release the S9 button to cause a reset event (while still holding S1 button).  

Make sure that the power switch (P3 in the [schematic](/Schematic/OS-2020-Badge-REV0-Schematic.PDF)) is in the 5V/Off position so that the serial dongle can provide power to the board.  

Connect the [CP2102 USB to Serial Dongle](/Images/USB-2-Serial-Dongle.jpg) to the P1 port of your board.  Note that the TXD pin of the dongle (trasmit) goes to the RXD-IN pin of the P1 header.  The RXD pin of the dongle connects to the TXD-OUT pin of the P1 header.  

Connect 5V and GND signals as well, it is not necessary to connect 3V3.  

While holding the S1 button, push and release the S9 button (to cause a reset event).  When S9 is released (and S1 is depressed during the process) the ESP32 will remain in bootloader mode.  

Upload your new code to the device by clicking the right facing arrow button under the menu bar or choose Sketch -> Upload

If all goes well you should see something like this in the output: 

       
        esptool.py v2.6-beta1
        Serial port /dev/ttyUSB0
        Connecting........_____...
        Chip is ESP32D0WDQ5 (revision 1)
        Features: WiFi, BT, Dual Core, 240MHz, VRef calibration in efuse, Coding Scheme None
        MAC: b4:e6:2d:d4:22:49
        Uploading stub...
        Running stub...
        Stub running...
        Configuring flash size...
        Auto-detected Flash size: 16MB
        Compressed 8192 bytes to 47...

        Writing at 0x0000e000... (100 %)
        Wrote 8192 bytes (47 compressed) at 0x0000e000 in 0.0 seconds (effective 7389.4 kbit/s)...
        Hash of data verified.
        Flash params set to 0x024f
        Compressed 17664 bytes to 11528...

        Writing at 0x00001000... (100 %)
        Wrote 17664 bytes (11528 compressed) at 0x00001000 in 1.0 seconds (effective 138.7 kbit/s)...
        Hash of data verified.
        Compressed 296208 bytes to 133929...

        Writing at 0x00010000... (11 %)
        Writing at 0x00014000... (22 %)
        Writing at 0x00018000... (33 %)
        Writing at 0x0001c000... (44 %)
        Writing at 0x00020000... (55 %)
        Writing at 0x00024000... (66 %)
        Writing at 0x00028000... (77 %)
        Writing at 0x0002c000... (88 %)
        Writing at 0x00030000... (100 %)
        Wrote 296208 bytes (133929 compressed) at 0x00010000 in 11.8 seconds (effective 200.3 kbit/s)...
        Hash of data verified.
        Compressed 3072 bytes to 144...

        Writing at 0x00008000... (100 %)
        Wrote 3072 bytes (144 compressed) at 0x00008000 in 0.0 seconds (effective 1374.7 kbit/s)...
        Hash of data verified.

        Leaving...
        Hard resetting via RTS pin...
       
    
You may receive a message about not having permission for /dev/ttyUSB0 if so then you can open terminal and type:

```sudo chmod a+rw /dev/ttyUSB0```

Once it is finished uploading the ESP32 is still in bootloader mode; to start the program you must manually reset the board by pressing the S9 button.  

## USB Upload

The OS2020 badge can accept code from a serial interface (P1 header) OR a USB interface (P5 header).  This section details the instructions for USB interface programming (P5).

To use the USB interface for programming you must solder on the provided [Adafruit USB-C Micro Lipo Module](/Images/Adafruit-Micro-Lipo-USB-C.jpg) and use the provided USB-C cable to upload code.  Instructions for installing the USB-C Micro Lipo module can be found in the beginning of the [battery tutorial](https://gitlab.com/stinky-fish/cuhsp-2021/-/blob/master/Tutorials/Battery-Tutorial.md).

When programming using the USB interface there is no need to push S1, S9 button sequence to put the ESP32 into upload mode because the 'USB-UART with AutoReset' section of the [schematic](/Schematic/OS-2020-Badge-REV0-Schematic.PDF) allows this to be done from software.  The U3 component is a [USB to Serial bridge](https://www.silabs.com/documents/public/data-sheets/cp2104.pdf).  The Q1/Q2 transistor network uses the DTR and RTS (serial) signals to assert the EN and RST pins.  

Make sure that the power switch (P3 in the [schematic](/Schematic/OS-2020-Badge-REV0-Schematic.PDF)) is in the 5V/Off position so that the USB port can provide power to the board.  

Connect one of the Raspberry Pi USB host ports to the USB-C device port on the PCB.  

Upload your new code to the device by clicking the right facing arrow button under the menu bar or choose Sketch -> Upload

If all goes well you should see something like this in the output: 

       
        esptool.py v2.6-beta1
        Serial port /dev/ttyUSB0
        Connecting........_____...
        Chip is ESP32D0WDQ5 (revision 1)
        Features: WiFi, BT, Dual Core, 240MHz, VRef calibration in efuse, Coding Scheme None
        MAC: b4:e6:2d:d4:22:49
        Uploading stub...
        Running stub...
        Stub running...
        Configuring flash size...
        Auto-detected Flash size: 16MB
        Compressed 8192 bytes to 47...

        Writing at 0x0000e000... (100 %)
        Wrote 8192 bytes (47 compressed) at 0x0000e000 in 0.0 seconds (effective 7389.4 kbit/s)...
        Hash of data verified.
        Flash params set to 0x024f
        Compressed 17664 bytes to 11528...

        Writing at 0x00001000... (100 %)
        Wrote 17664 bytes (11528 compressed) at 0x00001000 in 1.0 seconds (effective 138.7 kbit/s)...
        Hash of data verified.
        Compressed 296208 bytes to 133929...

        Writing at 0x00010000... (11 %)
        Writing at 0x00014000... (22 %)
        Writing at 0x00018000... (33 %)
        Writing at 0x0001c000... (44 %)
        Writing at 0x00020000... (55 %)
        Writing at 0x00024000... (66 %)
        Writing at 0x00028000... (77 %)
        Writing at 0x0002c000... (88 %)
        Writing at 0x00030000... (100 %)
        Wrote 296208 bytes (133929 compressed) at 0x00010000 in 11.8 seconds (effective 200.3 kbit/s)...
        Hash of data verified.
        Compressed 3072 bytes to 144...

        Writing at 0x00008000... (100 %)
        Wrote 3072 bytes (144 compressed) at 0x00008000 in 0.0 seconds (effective 1374.7 kbit/s)...
        Hash of data verified.

        Leaving...
        Hard resetting via RTS pin...
       
    
Some users report that the esptool has trouble connecting and times out during this step.  If this happens you can push and hold the S1 button when the esptool is trying to connect.  Doing this ensures that the processor is put in to program mode when the esptool is trying to connect.  The reason sequence is missed is because the EN pin is not being held low long enough after the reset pin is released (its happening too fast).  Users experiencing this behavior can add a 1 to 10 uF capacitor between the EN pin and GND to slow down the EN signal enough to make the esp32 programming sequence reliable such that an S1 button push is no longer required.      

Some users report an error message about not having permission for /dev/ttyUSB0.  If this happens you can open terminal and type:  
```sudo chmod a+rw /dev/ttyUSB0```

## Monitor
Arduino has a Serial Monitor feature that allows you to print messages from inside your code to understand if and how it is running, variable values, etc. 

Add a line of code your original setup function to call [Serial.begin()](https://www.arduino.cc/reference/en/language/functions/communication/serial/begin?from=Reference.BeginSerial) to start the serial monitor at a baud rate of 115200:

```  
void setup() {
  // initialize digital pin LED1 as an output
  pinMode(LED1, OUTPUT);
  // start the Serial Monitor at 115200 baud rate
  Serial.begin(115200);
}
```  

Next add a couple lines of code to the main loop to call [Serial.println()](https://www.arduino.cc/reference/en/language/functions/communication/serial/println) to print a message to the serial monitor each time you turn on or off the LED:

```
void loop() {
  digitalWrite(LED1, HIGH);   // turn the LED on (HIGH is the voltage level)
  Serial.println("LED ON");   // print "LED ON" message to the serial monitor
  delay(1000);                // wait for a 1000 milliseconds (1000 milliseconds equals 1 second)
  digitalWrite(LED1, LOW);    // turn the LED off by making the voltage LOW 
  Serial.println("LED OFF");  // print "LED OFF" message to the serial monitor
  delay(1000);                // wait for 1000 milliseconds
}
```
Upload and run your code.  Navigate to Tools -> Serial Monitor.  Make sure the Serial Monitor is set to 115200 baud rate in the lower right dialog box next.  



