# Simple Oscillator for the Cisco Offensive Summit Badge
(Special thanks to Sudhir Desai)

## Layout of Talk
- [Discuss the Code](#discuss-the-code)
	- [Set-up Arduino Compiler](#set-up-arduino-compiler)
	- [Open simple_oscilloscope.io](#open-simple_oscilloscope.io)
	- [Write simple_oscilloscope.io to Badge](#write-simple_oscilloscope.io-to-badge)
- [Assembling the External Components](#assembling-the-external-components)
	- [Build the Voltage Divider](#build-the-voltage-divider)
		- [Voltage Divider?](#voltage-divider?)
		- [Plug and Chug](#plugaand-chug)
	- [Build a Simple Waveform Generator Using a 555 Timer IC](#build-a-simple-waveform-generator-using-a-555-timer-ic)
- [fín](#fín)	


## Discuss the Code

### Set-up Arduino Compiler
To set up the Arduino Application and have a properly configured screen, please follow the instructions in the [LED Blink Tutorial](../Tutorials/LED-Blink-Tutorial.md) and the [Screen Text Tutorial](../Tutorials/Screen-Text-Tutorial.md).

### Open simple_oscilloscope.io
#### Copy and paste the below code into a new Arduino Sketch and save it:
```c++
// include tft driver and instantiate the tft
#include <TFT_eSPI.h>
TFT_eSPI tft = TFT_eSPI();
#define ledPin 22
#define screenX 128
#define screenY 128
const int TP34 = 34;
const int minX = 2;
int xVal = 2;

void setup() {
  tft.begin();
  tft.fillScreen(TFT_BLACK); 
  tft.drawFastHLine(0, 127, 127, TFT_WHITE);
  tft.drawFastVLine(0, 0, 127, TFT_WHITE);
}

void loop() {
  // Loop vars
  unsigned int inputVal = analogRead(TP34);
 
  // move pixel plotted
  if (xVal < 128) {
    xVal++;
  } else {
    xVal = minX;
  }

  // clear column for next pixel
  tft.drawFastVLine(xVal, 16, 111, TFT_BLACK);
  // calculate range then draw the pixel
  unsigned int yVal = 125 - (inputVal * 125 / 4095);
  tft.drawPixel(xVal, yVal, TFT_CYAN);
}
```

### Write simple_oscilloscope.io to Badge
To write the code to the badge, click the upload button (looks like a right-arrow in the toolbar).
![upload code to badge](../Images/simple-scope_upload_code_to_badge.png "upload code to badge")


## Assembling the External Components
![external components](../Images/simple-scope_badge_connections.png "external components")

### Build the Voltage Divider
#### Voltage Divider?
A voltage divider is a two-component method by which we can adjust the voltage range of an output so that we do not damage any components of our sampling device that are unable to handle the higher voltage of that output.<br />
![voltage divider ranges](../Images/simple-scope_voltage_divider-00.jpg "voltage divider ranges")
In our case, the 555 Timer Integrated Circuit outputs a voltage range of 0 to 5v DC; however, our badges are unable to handle more than 3.33 volts.<br />

#### Plug and Chug
This means we will need to rescale the 0-5v output to a more reasonable 0-3.333v level.<br />
Now that we know what our input and output ranges will be, we can decide on the resistances of the needed two resistors.<br />
![voltage divider equation and circuit diagram](../Images/simple-scope_voltage_divider-01.jpg "voltage divider equation and circuit diagram")
In my case, my go-to resistor (R<sub>2</sub>) has a value of 10,000 Ohms since I have many of them laying around.<br />
When we plug that into the formula, we see that the other resistor (R<sub>1</sub>) needs to be a 5,000 Ohm resistor.<br />
![voltage divider plug and chug](../Images/simple-scope_voltage_divider-02.jpg "voltage divider plug and chug")

#### The Circuit
Please note that since I have tons of 10kOhm resistors, I used one for both R<sub>1</sub> and R<sub>2</sub>:<br />
![voltage divider circuit](../Images/simple-scope_voltage_divider-circuit.png "voltage divider circuit")

### Build the Simple Waveform Generator Using a 555 Timer IC
The initial circuit we will sample is from https://www.electronics-tutorials.ws/waveforms/555_oscillator.html:<br />
![oscillator circuit](../Images/simple-scope_oscillator-circuit.png "oscillator circuit")

This 555 Oscillator will give us both a non-perfect sawtooth waveform:<br /> 
![sawtooth](../Images/simple-scope_sawtooth.gif "sawtooth")

...and a square (binary) waveform:<br />
![square](../Images/simple-scope_square.gif "square")

...depending on which part of the circuit we sample.


# fín
Thanks for attending this quick badge code example!
