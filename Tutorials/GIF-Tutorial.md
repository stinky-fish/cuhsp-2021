## GIF animation tutorial

Make sure you have done the [raspberry pi setup](Getting-Started-Raspberry-Pi.md) as well as the [arduino setup](Getting-Started-Arduino.md).

Make sure you have installed the screen and imported the TFT_eSPI library and modified your User_Setup.h as detailed in the [screen text tutorial](Screen-Text-Tutorial.md)

## Find an GIF animation

GIF means "Graphics Interchange Format"

Ponder if it should be pronounced "j-iff" or "g-iff" and maybe even [ask google](https://www.google.com/search?q=how+to+pronounce+gif&oq=how+to+pronounce+gif)

Use the world wide web to find a .gif image you would like to display on the screen. 

Save it to your Downloads folder and rename it to my_animation.gif

## GIF and 565 RGB format

GIF file format utilizes lossless compression that does not degrade the quality of the image.  

GIFs store image data using indexed color.  A standard GIF image can include a maximum of 256 colors.

Our Screen is a 128x128 pixel screen and supports a maximum of 65,000 colors (65k).

We'll need to convert each frame of our GIF file to the correct format for our screen - [RGB565](https://en.wikipedia.org/wiki/High_color)

RGB565 means that there are 5 bits of information for each red pixels, 6 bits of information for each blue pixel, and 5 bits of information for each blue pixel.

Lucky for us that some smart Cisco engineers have created a python script that can convert each frame of a GIF file from 256 color to RGB565 format that our screen understands

Download the [extractGifs.py](/Python/extractGifs.py) script and save it to your downloads folder as extractGifs.py.  

## Install Python, pip, and NumPY

Open a terminal window and use which to make sure we have python installed:

`which python3`

You should see a message returned that the environment knows python:

user@hostname:~$ which python
/usr/bin/python

If not then use apt to install python:
`sudo apt install python3`

The extractGifs.py script utililzes the [NumPy](https://numpy.org/) api

To install NumPY we need to use [pip](https://pip.pypa.io/en/stable/), which means "package installer for python"

Use which to make sure you have pip installed for python:

`which pip`

If not then install pip:

`sudo apt install python-pip`  

Once you have pip installed used it to install NumPy:

`pip install -U numpy`

If you get an error message that says 'error: externally-managed environment' then you can try this command:

`sudo pip install -U numpy --break-system-packages`

## Convert Gif

Navigate to your downloads folder:

`cd ~/Downloads`  

The extractGif.py script generates a header file for use in your code as well as binary file that contains the raw data for each image.

It creates a directory and extracts the GIF file (with multiple frames) into multiple single frame GIF files.  

Then it iterates over each single frame GIF file and resizes to 128x128 pixels while converting the 256 color data to RGB565 format.

Lastly it creates a header file that details the number of frames in the data (to be used in our code later) and writes the RGB565 data to a binary file.

extractGif.py needs 4 arguments to work:  input_gif_file_name, output_directory_name, output_binary_file_name, output_header_file_name

It will create the output directory if it does not exist.  Run the script to convert the data:

`python3 extractGifs.py ~/Downloads/my_animation.gif ~/Downloads/Converted my_animation.h my_animation.bin`  

If all goes well you should see the cursor eventually return and you will see the newly created directory.

## Create Sketch

Open the Arduino IDE and create a new Sketchbook.  

Navigate to File -> Save As and name it Arduino-GIF

Close Arduino.

Use a terminal to copy my_animation.gif and my_animation.bin to your Sketchbook folder:

`cp ~/Downloads/Converted/my_animation.bin ~/Arduino-Sketchbooks/Arduino-GIF`

`cp ~/Downloads/Converted/my_animation.h ~/Arduino-Sketchbooks/Arduino-GIF`

Re-open the Arduino IDE and open the Arduino-GIF.ino file in your Arduino-Sketchbooks folder.

You should now see 2 files, Arduino-GIF and my_animation.h

## Code 

Make sure you have imported the TFT_eSPI library and modified your User_Setup.h as detailed in the [screen text tutorial](Screen-Text-Tutorial.md)

To use the display we will utilize the custom [TFT_eSPI](https://github.com/Bodmer/TFT_eSPI) library, include these header files above the setup function:    

`#include <TFT_eSPI.h>       // library to drive screen`   
   
We will need to use the [esp_spi_flash](https://github.com/espressif/esp-idf/blob/master/components/spi_flash/include/esp_spi_flash.h) functions to read from the SPI flash memory in the WROVER  

`#include <esp_spi_flash.h>  // for reading our custom data from spi flash memory`  

We need to include our header file created by extractGifs.py to import the number of frames in the animation  

`#include "my_animation.h"   // Header File from extractGifs.py that defines num_frames for our custom screen data`  

Invoke the custom TFT_eSPI library after the include statements:  
`TFT_eSPI tft = TFT_eSPI();`  

Create a variable to set the delay time in between displaying each frame, more delay means slower animation, less delay means faster

`int delayTime = 80;`

Extend the setup function to initalize the screen & start the serial monitor: 
```
void setup(void) {

  Serial.begin(115200);           // Start serial monitor
  while(!Serial){}                // wait for it to complete
  
  tft.init();                     // initialize the screen
  tft.fillScreen(TFT_BLACK);      // fill it with black
  tft.setRotation(2);             // set the preferred rotation
  tft.setAddrWindow(0,0,128,128); // define window size
  //tft.setSwapBytes(1);          // Swap the byte order, uncomment to correct endianness if the things display incorrecly
  //tft.invertDisplay(1);         // Try to Invert the colors if they are wrong
}
```

Create a variable to keep track of what frame we are showing:  
`int y;                             // variable to index frames`

Create a variable to track our memory offset:  
`int offset = 0;                    // variable to index memory offset`

Create a 2D array to match a single frame for our 128 x 128 pixel screen size:  
`uint16_t data[128*128] = {0};      // initialize 2d array to match screen size`

Create a for loop that pulls a single frame of data and pushes it to the screen for the designate delayTime with each iteration:  

```

void loop() {

for (y = 0; y < num_frames; y++) {                        // loop over the number of frames
  Serial.print("Printing Frame # ");                      // write status message to serial monitor
  Serial.print(y);                                        // write current value to serial monitor
  Serial.print("\n");                                     // add a newline so next message is on the next line
 
  spi_flash_read(0x200000 + offset, data, sizeof(data));  // read custom image data from spi memory

  offset += 0x8000;                                       // increase byte offset to the beginning of the next frame:
                                                          // our display has 128 x 128 pixels
                                                          // each pixel is represented by 2 bytes of data
                                                          // 2 x 128 x 128 = 32768 bytes per frame
                                                          // 32768 in decimal converts to 0x8000 in hex 
                                 
  tft.pushImage(0, 0, 128, 128, data);                    // push data to the screen
  memset(data, 0, sizeof(data));                          // re-initialize data array to prepare for next read

  delay (delayTime);                                      // hold this image for the delay time
  if ( y == num_frames-1 ) {                              // if this is the last frame 
    delay(250);                                           // pause for 250 milliseconds
    offset = 0;                                           // reset the memory offset to repeat the movie
    }
  }
}


```

Compile your code.  Don't upload it yet b/c we still need to write our custom image data to SPI memory.  

## Write data to SPI Flash  

In order to write our data to the SPI flash memory we will use [esptool](https://github.com/espressif/esptool)  

Open a terminal and use pip to install esptool:  

`sudo pip install esptool`  

If you get an error message that says 'error: externally-managed environment' then you can try this command:

`sudo pip install esptool --break-system-packages`

You can show the help menu to make sure esptool is installed and working:

`esptool.py -h`

Navigate to your sketch directory:  

`cd ~/Arduino-Sketchbooks/Arduino-GIF`  

When writing our custom data to flash we must start at the address 0x200000 because we are using the ["Minimal 2MB Flash"](https://github.com/espressif/arduino-esp32/blob/master/tools/partitions/minimal.csv) partition scheme  

Custom data must be placed after any other data designated in the partition table. We observe that the last entry starts at 0x150000 with a size of 0xB0000  

This means our custom data can start at 0x150000 + 0xB000 = 0x200000  

Use esptool.py to write the custom image data to the SPI flash at address 0x200000  

`esptool.py --port /dev/ttyUSB0 write_flash 0x200000 my_animation.bin`  

## Upload Sketch

Once it is finished writing to flash memory, return to Arduino IDE and upload your sketch.  

If all goes well you should see your GIF appear and play.  

**Some GIF files may not work well due to the nature of their color format (which is not considered by our conversion script), but most of them should work just fine.  

More infromation about partition tables can be found [here](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-guides/partition-tables.html) 





 





