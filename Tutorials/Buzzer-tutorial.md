## Buzzer-Tutorial

Make sure you have done the [raspberry pi setup](Getting-Started-Raspberry-Pi.md) as well as the [arduino setup](Getting-Started-Arduino.md)  

Open the [schematic](/Schematic/OS-2020-Badge-REV0-Schematic.PDF) in a separate tab  

Locate the passive [buzzer speaker](https://www.thegeekpub.com/wiki/sensor-wiki-ky-006-passive-piezo-buzzer-module/) form your kit of stuff.  The passive buzzer speaker is the SHORTER speaker.  The TALLER buzzer speaker is 'active' and can only play a single frequency by applying power; not what we want.  We want to play with different frequencies in this tutorial. 

Note that the passive buzzer has 3 pins, but we only need to use 2 of them:  

(-)    ---------> for providing GND (return signal path) to the module    
middle ----------> (unused)  
right  ----------> for providing signal to the module   

In order to attach a buzzer we will need find an unused IO port in the schematic  

## Pick an IO port

Zoom into component U2 of the schematic and pick out an unused port. For this tutorial we'll use port IO9, which is connected to test point 9 (TP9) in th schematic.  Lets use the provided TP9 thruhole testpoint to install a header (or directly solder in a wire) and connect it to our buzzer.  

## Connect the wires

Connect TP9 net of the PCB to the IO pin of the buzzer module  

Connect GND net of the PCB to the GND net of the buzzer module  

When the IO signal is driven high, the buzzer membrane will move to its maximum  

When the IO signal is low, the buzzer membrane will move to its minimum  

The human ear can hear from about 50 Hertz or 50 Hz (meaning 50 cycles per second) until about 20 Kilo Hertz or 20KHz (meaning 20 x 1000 cycles per second)

The amazon website notes that this buzzer can produce tones up to 5 KHz  

## Code

Create a new Sketch

Navigate to File -> Save As and name it Arduino-Buzzer

To generate tones we will use the [esp32-hal-ledc](https://github.com/espressif/arduino-esp32/blob/a59eafbc9dfa3ce818c110f996eebf68d755be24/cores/esp32/esp32-hal-ledc.h) library for Pulse Width Modulation (PWM) control.  

Create variables for the frequency, Analog-to-Digital converter (ADC) channel, and ADC resolution:
```
int freq = 2000;       //Variable with a default frequency of 2kHz
int channel = 0;       //ADC channel 0
int resolution = 8;    //ADC resolution of 8 bits
```

Create a variable for the duty cycle, which controls volume by changing the amount of time the pin will is high in a given tone cycle

```
int duty_cycle = 128;  //4 of 8 bits or 50% duty cycle
```

Less cycles mean less average energy charges the speaker with each cycle and volume will be reduced (and vice versa)

Extend the setup function to start the serial monitor and setup the PWM parameters and assign the correct pin:

```
void setup() {
  
  Serial.begin(115200);                 //start  serial monitor at 115200 baud rate
  ledcSetup(channel, freq, resolution); //Use ADC channel 0, set default frequency, use 8 bits of resolution
  ledcAttachPin(9, channel);            //Assign to port 9, which is found at TP9 using ADC channel 0
  ledcWrite(channel, duty_cycle);       //Set the duty cycle to control volume
  
}

```
Create a loop to to iterate through the frequency range:
```
void loop() {
  for (int freq = 255; freq < 5000; freq = freq + 100){      // Start at 255 Hz and end at 5KHz increasing by 100Hz each time
  Serial.println(freq);                                      // Print current frequency to serial monitor
  ledcWriteTone(channel, freq);                              // Write the tone for the current iteration
  delay(500);                                                // Play 1 note every 500 milliseconds or 2 notes per second
  }
  
}

```
Compile and upload your code.  Enjoy the Tunes!!

Experiment with different duty cycles to play with different volumes..  

Here is a [wikipedia artice](https://en.wikipedia.org/wiki/Piano_key_frequencies) that shows frequency to note correlations

Here is a [fun site](https://noobnotes.net/popular/) with a bunch of popular melodies with individual note letters.

Use this information to make variables to represent notes and then code the notes you want in sequence to reproduce a tune.  

Happy Coding!



