from PIL import Image
import os, sys
import math
import traceback

def crop(path, input, width, height, k, directoryname, imgprefix, area, swap):
    im = Image.open(input)
    imgwidth, imgheight = im.size
    print("imgwidth %d imgheight %d" % (imgwidth, imgheight))
    for i in range(0,imgheight,height):
        for j in range(0,imgwidth,width):
            box = (j, i, j+width, i+height)
            a = im.crop(box)
            try:
                o = a.crop(area)
                newImage = Image.new('RGBA', (32,32), (255,0,0,0))
                x1 = int(math.floor((32 - imgwidth) / 2))
                y1 = int(math.floor((32 - imgheight) / 2))
                newImage.paste(o, (0,0))
                newImage = newImage.resize((128,128), Image.NEAREST)
                if (swap is True):
                    newImage = newImage.transpose(Image.FLIP_LEFT_RIGHT)
                filename = os.path.join(path,"PNG","%s" % directoryname,"%s%04d.png" % (imgprefix, k))
                print("[+] saving %s" % filename)
                newImage.save(filename)
            except Exception as e:
                traceback.print_exc()
                pass
            k +=1
    return k

try:
   os.mkdir("./PNG/")
except:
   pass
   
for infile in sys.argv[1:]:
   directoryname= infile.split('.')[:-1][0]
   try:
      os.mkdir("./PNG/" + directoryname)
   except:
      pass
   index = crop('./', infile, 32, 32, 0, directoryname, directoryname, (0, 0, 32, 32),False)
for infile in sys.argv[1:]:
   directoryname= infile.split('.')[:-1][0]
   crop('./', infile, 32, 32, index, directoryname, directoryname, (0, 0, 32, 32),True)
