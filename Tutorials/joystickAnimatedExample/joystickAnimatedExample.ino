#include <SPI.h>
#include <TFT_eSPI.h>       // Hardware-specific library, be sure to have correctly edited /Users/XXXX/Documents/Arduino/libraries/TFT_eSPI/User_Setup.h 
#include <string.h>
#include "image_arrays.h"

int last_orientation = 0;
int previous_state = 0; 
int initialized = 0;
int frame_number = 0;

TFT_eSPI tft = TFT_eSPI();  // Invoke custom library


#define DELAY_TIME 50      // 50ms seems good
#define CENTER_STATE 0
#define UP_STATE 12
#define DOWN_STATE 6
#define DOWN_LEFT 8
#define DOWN_RIGHT 4
#define RIGHT_STATE 3
#define LEFT_STATE 9
#define FACING_LEFT 2
#define FACING_RIGHT 1


void setup(void) 
{
  Serial.begin(115200);
  while(!Serial){}  
  tft.init();
  tft.fillScreen(TFT_BLACK);
  tft.setTextSize(2);
  tft.setAddrWindow(0,0,128,128);
  tft.setSwapBytes(1);     // Swap the byte order for pushImage() - corrects endianness 
  tft.pushImage(0,0,128,128,character_right_center);
}


int detect_up(int x, int y)
{
  int returnValue = 0;
  if ( (y >= 0 ) && (y <= 500) &&
       (x < 1900) && (x > 1800) ) {
       returnValue = 1;
  }
  return returnValue;  
}

int detect_down(int x, int y)
{
  int returnValue = 0;
  if ( (y > 3900 ) &&
      (x < 1900) && (x > 900) ) {
      returnValue = 1;
  }
  return returnValue;  
}

int detect_y_center(int x, int y)
{
  int returnValue = 0;
  if ((y < 1600) && (y > 1800) )
    returnValue = 1;
  return returnValue;
}

int detect_x_center(int x, int y)
{
  int returnValue = 0;
  if ((x < 1900) && (x > 1600))
    returnValue = 1;
  return returnValue;
}

int detect_x_left(int x, int y)
{
  int returnValue = 0;
  if (x > 4000)
    returnValue = 1;
  return returnValue;
}

int detect_y_down(int x, int y)
{
  int returnValue = 0;
  if (y > 4000)
    returnValue = 1;
  return returnValue;
}


int detect_y_up(int x , int y)
{
  int returnValue = 0;
  if ((y >= 0 ) && (y <= 100))
    returnValue = 1;
  return returnValue;
}

int detect_x_right(int x , int y)
{
  int returnValue = 0;
  if ((x >= 0 ) && (x <= 100))
    returnValue = 1;
  return returnValue;
}

void animate_right()
{
  int x;
  int array_length = sizeof(right_img_pointer) >> 2; 
  for(x = 0; x < array_length; x++) {
      Serial.print("Printing Right Frame # ");
      Serial.print(x);
      Serial.print("\n");
      tft.pushImage(0,0,128,128,right_img_pointer[x]);
      delay (DELAY_TIME);   
      last_orientation = FACING_RIGHT;
  }
}

void animate_left()
{
  int x;
  int array_length = sizeof(left_img_pointer) >> 2; 
  for(x = 0; x < 5; x++) {
      Serial.print("Printing Left Frame # ");
      Serial.print(x);
      Serial.print("\n");
      tft.pushImage(0,0,128,128,left_img_pointer[x]);
      delay (DELAY_TIME);   
      last_orientation = FACING_LEFT;
  }
}

void animate_up_left()
{
  int x;
  int array_length = sizeof(up_left_img_pointer) >> 2;
  
  for(x = 0; x < array_length; x++) {
      Serial.print("Printing Up Left Frame # ");
      Serial.print(x);
      Serial.print("\n");
      tft.pushImage(0,0,128,128,up_left_img_pointer[x]);
      delay (DELAY_TIME);   
      last_orientation = FACING_LEFT;
  }
}
void animate_up_right()
{
  int x;
  int array_length = sizeof(up_right_img_pointer) >> 2;
  for(x = 0; x < array_length; x++) {
      Serial.print("Printing Up Right Frame # ");
      Serial.print(x);
      Serial.print("\n");
      tft.pushImage(0,0,128,128,up_right_img_pointer[x]);
      delay (DELAY_TIME);   
      last_orientation = FACING_RIGHT;
  }
}


void loop() 
{
  
  int analog_value_x = 0;
  int analog_value_y = 0;
  
  analog_value_x = analogRead(34);
  analog_value_y = analogRead(35);
  if ( (initialized == 0) && (analog_value_x == 0) && (analog_value_y == 0) )
  {
    tft.drawString("No Power?", 5, 50);
  }
  else
  {
    initialized = 1;
  }
 
  if (detect_y_up(analog_value_x, analog_value_y))
  {
    if (detect_x_center(analog_value_x, analog_value_y))
    {
//      if (previous_state != UP_STATE)
      {
        Serial.print("UP\n");
        previous_state = UP_STATE;
        if (last_orientation == FACING_LEFT)
            animate_up_left();
        else
            animate_up_right();
      }
    }
    else if (detect_x_left(analog_value_x, analog_value_y))
    {
//      if (previous_state != LEFT_STATE)
      {
        Serial.print("UP LEFT\n");
        animate_up_left();
        last_orientation = FACING_LEFT;
        previous_state = LEFT_STATE;
      }
    }
    else if (detect_x_right(analog_value_x, analog_value_y))
    {
//      if (previous_state != FACING_RIGHT)
      {
        Serial.print("UP RIGHT\n");
        animate_up_right();
        last_orientation = FACING_RIGHT;
        previous_state = RIGHT_STATE;
      }
    }
  }
  else if (detect_y_down(analog_value_x, analog_value_y))
  {
    if (detect_x_center(analog_value_x, analog_value_y))
    {
      if (previous_state != DOWN_STATE)
      {
        Serial.print("DOWN\n");
        previous_state = DOWN_STATE;
        if (last_orientation == FACING_LEFT)
          tft.pushImage(0,0,128,128,character_left_down);
        else
          tft.pushImage(0,0,128,128,character_right_down);
      }
    }
    else if (detect_x_right(analog_value_x, analog_value_y))
    {
      if (previous_state != DOWN_RIGHT)
      {
        Serial.print("DOWN RIGHT\n");
        tft.pushImage(0,0,128,128,character_right_down);
        last_orientation = FACING_RIGHT;
        previous_state = DOWN_RIGHT;
      }
    }
    else if (detect_x_left(analog_value_x, analog_value_y))
    {
      if (previous_state != DOWN_LEFT)
      {
        Serial.print("DOWN LEFT\n");
        tft.pushImage(0,0,128,128,character_left_down);
        last_orientation = FACING_LEFT;
        previous_state = DOWN_LEFT;
      }
    }
  }
  else if (detect_x_left(analog_value_x, analog_value_y))
  {
      Serial.print("LEFT\n");
      animate_left();
      last_orientation = FACING_LEFT;
      previous_state = LEFT_STATE;
  }
  else if (detect_x_right(analog_value_x, analog_value_y))
  {
      Serial.print("RIGHT\n");
      animate_right();
      last_orientation = FACING_RIGHT;
      previous_state = RIGHT_STATE;
  }
  else
  {
    if (previous_state != CENTER_STATE)
    {
      Serial.print("CENTER\n");
      if (last_orientation == FACING_LEFT)
        tft.pushImage(0,0,128,128,character_left_center);
      else
        tft.pushImage(0,0,128,128,character_right_center);
      previous_state = CENTER_STATE;
    }
  }
 /* 
  * WARNING: The flashing from this text may cause a seizure
  tft.fontHeight(2);
  tft.drawString("     ", 0, 10);
  tft.drawString("     ", 80, 10);
  
  tft.drawNumber(analog_value_x, 0, 10);
  tft.drawNumber(analog_value_y, 80, 10);
*/
 
   Serial.print("x: ");
   Serial.print(analog_value_x);
   Serial.print("y: ");
   Serial.print(analog_value_y);
   Serial.print("\n");
 
}
