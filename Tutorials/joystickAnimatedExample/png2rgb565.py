#!/usr/bin/python

import sys
import os
from PIL import Image
from PIL import ImageDraw
import struct
import re

isSWAP = False

def main():

    len_argument = len(sys.argv)
    filesize = 0
    
    for infile in sys.argv[1:]:
        try:
            im=Image.open(infile)
            #print ("/* Image Width:%d Height:%d */" % (im.size[0], im.size[1]))
        except:
            print ("Fail to open png file ", sys.argv[1])
            sys.exit(0)

        image_height = im.size[1]
        image_width = im.size[0]

        outfilename = os.path.splitext(infile)[0] + ".h"
        try:
            outfile = open(outfilename,"w")
        except:
            print ("Can't write the file %s" % outfilename)
            sys.exit(0)

        #    try:
        #        binoutfile = open(os.path.splitext(infile)[0] + ".bin","wb")
        #    except:
        #        print ("Can't write the binary file %s" % os.path.splitext(infile)[0] + ".bin")
        #        sys.exit(0)


        print ("/* Image Width:%d Height:%d */" % (im.size[0], im.size[1]), file=outfile)
        variable = os.path.basename(infile.split('.')[:-1][0]).replace(' ', '_')
        print ("const static uint16_t " + variable + "[] = {", file=outfile)

        pix = im.load()  #load pixel array
        for h in range(image_height):
            for w in range(image_width):
                if ((h * 16 + w) % 16 == 0):
                    print (" ", file=outfile)
                    print ("\t\t", file=outfile, end = '')

                if w < im.size[0]:
                    R=pix[w,h][0]>>3
                    G=pix[w,h][1]>>2
                    B=pix[w,h][2]>>3

                    rgb = (R<<11) | (G<<5) | B

                    if (isSWAP == True):
                        swap_string_low = rgb >> 8
                        swap_string_high = (rgb & 0x00FF) << 8
                        swap_string = swap_string_low | swap_string_high
                        print ("0x%04x," %(swap_string), file=outfile, end = '')
        #                  binoutfile.write(struct.pack('H', swap_string))
                    else:
                        print ("0x%04x," %(rgb), file=outfile, end = '')
        #                  binoutfile.write(struct.pack('H', rgb))
                else:
                    rgb = 0
            #
        print ("", file=outfile)
        print ("};", file=outfile)

        outfile.close()
        #    binoutfile.close()

        print ("PNG file \"%s\"" % infile + " written out to header file \"%s\"" % outfilename)

if __name__=="__main__":
  main()
