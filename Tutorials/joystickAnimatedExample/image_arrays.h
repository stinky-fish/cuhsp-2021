#include "PNG/fox/fox0014.h"
#include "PNG/fox/fox0015.h"
#include "PNG/fox/fox0016.h"
#include "PNG/fox/fox0017.h"
#include "PNG/fox/fox0018.h"

#include "PNG/fox/fox0057.h" // up right
#include "PNG/fox/fox0058.h" // up right
#include "PNG/fox/fox0059.h" // keyframe up right
#include "PNG/fox/fox0060.h" // up right
#include "PNG/fox/fox0061.h" // up right
#include "PNG/fox/fox0062.h" // up right
#include "PNG/fox/fox0063.h" // up right
#include "PNG/fox/fox0064.h" // up right
#include "PNG/fox/fox0065.h" // up right
#include "PNG/fox/fox0066.h" // up right


#include "PNG/fox/fox0084.h" // laydown right

#include "PNG/fox/fox0126.h"
#include "PNG/fox/fox0127.h"
#include "PNG/fox/fox0128.h"
#include "PNG/fox/fox0129.h"
#include "PNG/fox/fox0130.h"

#include "PNG/fox/fox0169.h" // up left
#include "PNG/fox/fox0170.h" // up left
#include "PNG/fox/fox0171.h" // keyframe up left
#include "PNG/fox/fox0172.h" // up left
#include "PNG/fox/fox0173.h" // up left
#include "PNG/fox/fox0174.h" // up left
#include "PNG/fox/fox0175.h" // up left
#include "PNG/fox/fox0176.h" // up left
#include "PNG/fox/fox0177.h" // up left
#include "PNG/fox/fox0178.h" // up left


#include "PNG/fox/fox0196.h" // laydown left


const static uint16_t *character_right_center  = fox0014;
const static uint16_t *character_right_right01 = fox0015;
const static uint16_t *character_right_right02 = fox0016;
const static uint16_t *character_right_right03 = fox0017;
const static uint16_t *character_right_right04 = fox0018;


const static uint16_t *character_right_up00 = fox0059;

const static uint16_t *character_right_down00 = fox0084;

const static uint16_t *character_left_center   = fox0126;
const static uint16_t *character_left_left01 = fox0127;
const static uint16_t *character_left_left02 = fox0128;
const static uint16_t *character_left_left03 = fox0129;
const static uint16_t *character_left_left04 = fox0130;


const static uint16_t *character_left_up00 = fox0171;
const static uint16_t *character_left_down00 = fox0196;


const static uint16_t *character_up_left00 = fox0169;
const static uint16_t *character_up_left01 = fox0170;
const static uint16_t *character_up_left02 = fox0171;
const static uint16_t *character_up_left03 = fox0172;
const static uint16_t *character_up_left04 = fox0173;
const static uint16_t *character_up_left05 = fox0174;
const static uint16_t *character_up_left06 = fox0175;
const static uint16_t *character_up_left07 = fox0176;
const static uint16_t *character_up_left08 = fox0177;
const static uint16_t *character_up_left09 = fox0178;


const static uint16_t *character_up_right00 = fox0057;
const static uint16_t *character_up_right01 = fox0058;
const static uint16_t *character_up_right02 = fox0059;
const static uint16_t *character_up_right03 = fox0060;
const static uint16_t *character_up_right04 = fox0061;
const static uint16_t *character_up_right05 = fox0062;
const static uint16_t *character_up_right06 = fox0063;
const static uint16_t *character_up_right07 = fox0064;
const static uint16_t *character_up_right08 = fox0065;
const static uint16_t *character_up_right09 = fox0066;

const uint16_t *right_img_pointer[] = {character_right_center, character_right_right01, character_right_right02, character_right_right03, character_right_right04  };
const uint16_t *left_img_pointer[]  = {character_left_center, character_left_left01, character_left_left02, character_left_left03, character_left_left04  };
//const uint16_t *up_left_img_pointer[] = {character_left_center, character_up_left00, character_up_left01, character_up_left02, character_up_left03,character_up_left04,character_up_left05,character_up_left09 };
//const uint16_t *up_right_img_pointer[] = {character_right_center, character_up_right00, character_up_right01, character_up_right02, character_up_right03,character_up_right04,character_up_right05,character_up_right09 };

const uint16_t *up_left_img_pointer[] = {character_left_center, character_up_left00, character_up_left01, character_up_left02, character_up_left03,character_up_left04,character_up_left05,character_up_left06,character_up_left07, character_up_left08, character_up_left09 };
const uint16_t *up_right_img_pointer[] = {character_right_center, character_up_right00, character_up_right01, character_up_right02, character_up_right03,character_up_right04,character_up_right05,character_up_right06,character_up_right07,character_up_right08,character_up_right09 };


const uint16_t *character_left_down = { character_left_down00 };
const uint16_t *character_right_down = { character_right_down00  };
