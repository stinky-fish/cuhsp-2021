from PIL import Image
import os, sys
import math
import traceback


for infile in sys.argv[1:]:
    im = Image.open(infile)
    imgwidth, imgheight = im.size
    mode = im.mode
    if len(mode) == 1:  # L, 1
        new_background = (255)
    if len(mode) == 3:  # RGB
        new_background = (255, 255, 255)
    if len(mode) == 4:  # RGBA, CMYK
        new_background = (255, 255, 255, 255)

    newImage = Image.new(mode, (imgwidth, imgheight+19), new_background)
    newImage.paste(im, (0,19))
    new_image_path = infile.split('.')[:-1][0]
#    newImage.save(new_image_path + 'resized' + '.png')
    newImage.save('fox.png')
