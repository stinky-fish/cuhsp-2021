This project will require a sprite sheet from https://elthen.itch.io/2d-pixel-art-fox-sprites


After downloading the image, run the resize.py script. 

`python3 resize.py "Fox Sprite Sheet.png"`

The resize script will generate a fox.png file. 

After that the fox.png file is cropped into smaller files using the crop.py script. The crop.py script will also create mirror images of the sprites to have left oriented images. 

`python3 crop.py fox.png`

The crop script will create a PNG directory with the first part of the name of the file (fox).

Finally, the png2rgb555.py script is run on PNG/fox/*.png to generate a series of header files which are included in image_arrays.h. 

`python3 png2rgb555.py PNG/fox/*.png`

The code may need to be updated to recalibrate the joystick.

