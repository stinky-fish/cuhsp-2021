## Screen Text Tutorial
Make sure you have done the [raspberry pi setup](Getting-Started-Raspberry-Pi.md) as well as the [arduino setup](Getting-Started-Arduino.md).

Open the [schematic](/Schematic/OS-2020-Badge-REV0-Schematic.PDF) in a separate tab.

In the Display area of the circuit we can see an 8 pin connector (component P2) where the screen attaches to the PCB.  

| Display Pin  | Name  | Description  | U2 Port |
|----|------|--------------------------------------------------------|---------|
| 1  | GND  | attached to the 'ground net' which is the power supply return path for all currents  | none, pins 1 & 38 |
| 2  | VCC  |  'voltage common collector' - just a fancy word that means the power net, connected to 3.3V net which is the output of our 3.3V regulator U1 | none, pin 2 |
| 3  | SCL  |  'serial clock' - this is the clock signal used to send data to the screen | IO18 |
| 4  | SDA  |  'serial data' - this is the data signal which is sampled by the display each time the clock signal transitions | IO23 |
| 5  | RES  |  'reset' - use this signal to reset the screen to a known state or hold the display in reset | IO25 |
| 6  | DC   |  'data/command' to designate if we are communicating data or a command | IO26 |
| 7  | CS   |  'chip select' to select this display for receiving data, it will not listen if it is not selected | IO19 |
| 8  | BL   |  'backlight' to brighten or dim the screen according on the average voltage value of this net | IO5 |

## Install Screen

Solder the provided [100 mil receptacle](/Images/2.54mm-receptacle.jpg) into the P2 component pads on the [top side of the PCB](/Images/OS-2020-Badge-REV0-3D-Render-Front.png). 

Install 2 of the [provided standoffs](/Images/PCB-Standoff.jpg) near the bottom of the P2 component outline by inserting 2 screws into the [bottom side of the printed circuit board](/Images/OS-2020-Badge-REV0-3D-Render-Back.png).  

Solder the [100 mil header pins](/Images/2.54mm-header.jpg) onto the screen (this header part should have come in the screen bag).

Install the screen, fasten it by inserting two more screws into the newly placed standoffs.

Once you are all finished it shoud look [something like this](/Images/Screen-Installed.jpg).  

## Import libraries

Lucky for us some smart people on the internet have written code to make it easy for us to drive these pins correctly.  We'll be using the [TFT_eSPI library](https://github.com/Bodmer/TFT_eSPI) 

Create a new sketch using File -> "New"

Navigate to File -> Save As and name it Arduino-Screen-Text

Navigate to Sketch -> "Include Library" -> "Manage Libraries"

Filter your search using "TFT_eSPI" and install the latest version.

## Modify User_setup.h

The TFT_eSPI library does not know which pins we are using to drive the screen.  

To designate these pins we must modify the User_Setup.h file that was created when we imported the library.  

Open a terminal window and use the find program to search your home directory for the location of the User_setup.h file:  

`sudo find ~ -type f -name "User_Setup.h"`  

Use the terminal to open the nano editor and modify the User_Setup.h file using the path you just found with the find command:  

`nano /home/pi/Arduino-Sketchbooks/libraries/TFT_eSPI/User_Setup.h`  

Our display is based on the [ST7735](https://www.displayfuture.com/Display/datasheet/controller/ST7735.pdf)  

Our screen does not use the ILI9341 driver; find and comment out this line of code by adding two slashes in the front:  

`//#define ILI9341_DRIVER`  

Our screen will use the ST7735 driver; find and uncomment this line of code by removing the two slashes in the front:  

`#define ST7735_DRIVER `  

Your screen likely uses a color order of Blue-Green-Red.  Uncomment this line to set the correct color order by removing the two slashes in the front:

`#define TFT_RGB_ORDER TFT_BGR`

Our screen comes with a green colored tab on the removable protection film.  Our screen is 128 x 128 pixels.  Locate and uncomment these lines of code by removing the two slashes in front:  

`#define TFT_WIDTH 128`    

`#define TFT_HEIGHT 128`  

`#define SST7735_GREENTAB128`  

Scroll down and locate the section 'EDIT THE PIN NUMBERS IN THE LINES FOLLOWING TO SUIT YOUR ESP32 SETUP'  

Uncomment and correct the pin numbers to match our schematic:  

`//#define TFT_MISO 19  // Our display does not use MISO - Primary Input Secondary Output`    
`#define TFT_MOSI 23    // MOSI - Primary Output Secondary Input`  
`#define TFT_SCLK 18    // SCLK - serial clock`  
`#define TFT_CS   19    // Chip select control pin`  
`#define TFT_DC   26    // Data Command control pin`   
`#define TFT_RST  25    // Reset pin`  
`#define TFT_BL   5     // LED back-light`  

Save this file!  

## Code  

Open a new sketch

Save it as Arduino-Screen-Text.ino

To use the display we will utilize the custom [TFT_eSPI](https://github.com/Bodmer/TFT_eSPI) library, include this header file above the setup function:  
`#include <TFT_eSPI.h>`  

Invoke the custom TFT_eSPI library after the include statements:  
`TFT_eSPI tft = TFT_eSPI();`  

Extend the setup function to initalize the screen:  
```
void setup(void) {
  tft.init();  //intialize the screen
  tft.fillScreen(TFT_BLACK);               // Fill it with black
  tft.setCursor(0, 0, 4);                  // Set "cursor" at top left corner of display (0,0) and select font 4
  tft.setTextColor(TFT_WHITE, TFT_BLACK);  // Set the font colour to be white with a black background
  tft.println("Its Alive! \n");            // Plot text on screen using the "print" class and append with a newline (\n)
  delay(5000);                             // Hold this image for 5 secs
}

```

Define a counter variable.  Use it in the main loop function to experiment with text while incrementing the cursor position every 1/10 seconds.  Once it exits the screen reset it.  

```
int i = 0; 

void loop() {

  tft.setCursor(0, i, 4);                   // set cursor position and select font 4

  tft.setTextColor(TFT_WHITE, TFT_BLACK);   // set text color to white on black background
  tft.println("White text ");               // print status to serial monitor
  
  tft.setTextColor(TFT_RED, TFT_BLACK);     // set text color to red on black background
  tft.println("Red text     ");             // print status to serial monitor
  
  tft.setTextColor(TFT_GREEN, TFT_BLACK);   // set text color to green on black background
  tft.println("Green text ");               // print status to serial monitor
  
  tft.setTextColor(TFT_BLUE, TFT_BLACK);    // set text color to blue on black background
  tft.println("Blue text  ");               // print status to serial monitor
  
  delay(100);                               // hold this image for 1/10 of a second (100 milliseconds)
  
  i++;                                      // increment our counter variable to move the text down slightly in the next execution
  
  if (i > 127) {
      i = 0;                                // start all over once the text scrolls off the bottom of the screen
      tft.fillScreen(TFT_BLACK);            // blank the screen
  }
}


```

Compile, upload, and run your code!!  
