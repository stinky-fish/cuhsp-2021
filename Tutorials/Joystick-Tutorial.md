## Joystick tutorial

Make sure you have done the [raspberry pi setup](Getting-Started-Raspberry-Pi.md) as well as the [arduino setup](Getting-Started-Arduino.md).


The tutorial will use the Serial port
WARNING: using the screen for the updates may cause an epiletic seizure due to the flashing text update.

The TFT_eSPI library is nice, but not completely necessary. That tutorial can be found here: [screen text tutorial](Screen-Text-Tutorial.md)

## Find two analog pins in the [schematic](/Schematic/OS-2020-Badge-REV0-Schematic.PDF) 

In this case, this tutorial is using TP34 & TP35

## Wire up the controller

You can use the 3.3V line to the 5v line on the controller. Connect the GND pin on the joystick to a GND pin on the badge.  Setup VRX and VRY to be either TP34 and TP35. In this example, VRx is TP34 and VRy is TP35.

## Mount the controller on the badge (Optional)
Figure out a way to keep the controller steady. I found it best to use the feet that came with the badge and inverted one then used another one to keep a two leg mount.

## Create Sketch

Open the Arduino IDE and create a new Sketchbook.  
Navigate to File -> Save As and name it Arduino-Joystick


## Code 

Extend the setup function to initalize the screen & start the serial monitor: 
```
void setup(void) {
  Serial.begin(115200);           // Start serial monitor
}
```

Update the loop to read and print the values


```

void loop() {
  Serial.print("VRx: ");         
  Serial.print(analogRead(34));
  Serial.print("\t\t");  // this line adds tabs to make the output look nice
  Serial.print("VRy: ");
  Serial.print(analogRead(35));
  Serial.print("--------------------\n");  
}

```

## Compile and Upload Sketch

Use the Arduino IDE and upload your sketch.  

If all goes well you should see output in the serial monitor which should change as you move around the joystick. If the values being read are simply 0, check to see that power is properly hooked up.





