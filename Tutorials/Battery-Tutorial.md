## Li-Ion Battery Tutorial

### Install Adafruit Micro Lipo Module

Locate the [18650 style Lithium-Ion battery](/Images/18650-battery.jpg) form your kit.  

Locate the [Adafruit Micro Lipo USB-C](/Images/Adafruit-Micro-Lipo-USB-C.jpg) circuit from your kit.

Open the [schematic](/Schematic/OS-2020-Badge-REV0-Schematic.PDF) in a separate tab  

Zoom into the 'USB-C' section of the schematic.  Note the header with designator P5, this is where the [Adafruit Micro Lipo USB-C](/Images/Adafruit-Micro-Lipo-USB-C.jpg) will be installed.  

The P5 header can be found on the [bottom side of the board](/Images/OS-2020-Badge-REV0-3D-Render-Back.png), just look for the "P5" designator (upside-down in the picture, on the right edge of the board).

Solder the provided [100 mil header](/Images/2.54mm-header.jpg) into the P5 component pads on the bottom side of the PCB. 

Install 4 of the [provided standoffs](/Images/PCB-Standoff.jpg) around the P5 compnent by inserting 4 screws into the [top side of the printed circuit board](/Images/OS-2020-Badge-REV0-3D-Render-Front.png).  

Use the 4 remaining screws to install the [Adafruit Micro Lipo USB-C](/Images/Adafruit-Micro-Lipo-USB-C.jpg) onto the 100-mil header.

Solder the 100 mil header pins into the Adafruit Micro Lipo USB-C module.  

When you're all finished soldering the Adafruit Micro Lipo module it should look [something like this](/Images/Micro-Lipo-Installed.jpg)

### Set the charge current
Here are the [technical details for the Adafruit Micro Lipo USB-C solution](https://learn.adafruit.com/adafruit-microlipo-and-minilipo-battery-chargers/downloads).

This board uses the [MCP73831 charger IC](https://cdn-shop.adafruit.com/datasheets/MCP73831.pdf) and has a default charge rate of 100 milli-Amps (mA).

You can optionally increase the charge rate from 100mA to 500mA (and decrease the charge time by ~5x) by soldering together the two jumper pads below the '500mA' label on the Adafruit Micro-Lipo board.

### Install the battery

Zoom into 'Power' section of the schematic, note the BT2 header.  This is the holder for the provided 18650 Lithium Ion battery.  

Install the Lithium Ion battery into the BT2 battery holder.  Pay attention to the positive and minus markings on the battery and on the battery holder; be sure to install the battery the correct way.

Plug in to a USB power source and the charge LED (light emitting diode located near "CHG") should illuminate.  Congratulations - you are charging your battery!

The battery will charge anytime USB power is connected.  The MCP73831 will take care to charge your battery safeley and will automatically stop when its done.  

Once it is charged you can unplug the cables and walk around with your badge.  Battery life will depend on how much power (computing/communication, etc.) you are doing in your coded solution.  

Using the P3 switch located on the [top side of the printed circuit board](/Images/OS-2020-Badge-REV0-3D-Render-Front.png) you can select whether the board draws power from 5V (taken from either the P5 USB header or the P1 programming header) OR from the battery.  

If there is no power connected to the P5 USB header or P1 programming header then the device will turn off when the P3 switch is moved to the 5V/OFF position.    
