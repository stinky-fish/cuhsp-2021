## Touch Button Tutorial

Make sure you have done the [raspberry pi setup](Getting-Started-Raspberry-Pi.md)

Make sure you have done the  [arduino setup](Getting-Started-Arduino.md)

Make sure you have installed the screen and imported the TFT_eSPI library and modified your User_Setup.h as detailed in the [screen text tutorial](Screen-Text-Tutorial.md)

Open the [schematic](/Schematic/OS-2020-Badge-REV0-Schematic.PDF) in a separate tab.

The ESP WROVER provides capacitive sense button feature.

In the PCB Cap Sense area of the circuit we can see 8 buttons. 

Following the net names we can see which ports the buttons are connected to:

| Button  | Net Name  | WROVER Port | 
|----|------|-----|
| S0  | TOUCH0  | 4  |
| S2  | TOUCH1  | 2  |
| S3  | TOUCH3  | 15 |
| S4  | TOUCH4  | 13 |
| S5  | TOUCH5  | 12 |
| S6  | TOUCH6  | 14 |
| S7  | TOUCH7  | 27 |
| S8  | TOUCH8  | 33 |


## Code  

Lucky for us some smart people at espressif have provided a [touchRead()](https://github.com/espressif/arduino-esp32/blob/master/cores/esp32/esp32-hal-touch.h#L43) function for us to use.

Open a new sketch

Navigate to File -> Save As and name it Arduino-Touch-Button

At the top of the file, define some constant integer values named after our buttons and assign the port numbers to make it easier to code:

```
const int S0 = 4;     //S0 is connected to port 4  
const int S2 = 2;     //S2 is connected to port 2
const int S3 = 15;    //S3 is connected to port 15
const int S4 = 13;    //S4 is connected to port 13
const int S5 = 12;    //S5 is connected to port 12
const int S6 = 14;    //S6 is connected to port 14
const int S7 = 27;    //S7 is connected to port 27
const int S8 = 33;    //S8 is connected to port 33 but for modules we may need to use 32 because there is a bug; see https://github.com/espressif/arduino-esp32/issues/3584
```

Extend the setup function to start the serial monitor and print a starting message:

```

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);             // start the serial monitor at 115200 baud
  Serial.println("Button Test!");   // print a startup message
  delay(1000);                      // hold for a one second (1000 milliseconds) to be able read the start message
}


```

Extend the main loop to read the value of each touch button and print it to the serial monitor twice a second:

```
void loop() {
  // put your main code here, to run repeatedly:

  Serial.print("S0 touchRead value is ");
  Serial.println(touchRead( S0 ));  // get value of S0

  Serial.print("S2 touchRead value is ");
  Serial.println(touchRead(S2));  // get value of S2

  Serial.print("S3 touchRead value is ");
  Serial.println(touchRead(S3));  // get value of S3

  Serial.print("S4 touchRead value is ");
  Serial.println(touchRead(S4));  // get value of S4

  Serial.print("S5 touchRead value is ");
  Serial.println(touchRead(S5));  // get value of S5

  Serial.print("S6 touchRead value is ");
  Serial.println(touchRead(S6));  // get value of S6

  Serial.print("S7 touchRead value is ");
  Serial.println(touchRead(S7));  // get value of S7

  Serial.print("S8 touchRead value is ");
  Serial.println(touchRead(S8));  // get value of S8

  delay(500);  //sample values every 1/2 second (500 milliseconds)

  // clear a bunch of space to make it easier to read each sample in the monitor window
  for (int i = 0; i <= 50; i++) {
    Serial.println("");
  }

}

```

Upload and run your code.  Experiment with touching each button.  Note the maximum and minimum values for each button when you are pressing it or not.

For my board I observe in the serial monitor that the lowest value when no buttons are pressed is 37, and the hightest value when all buttons are pressed is 19.

So any value less than 20 means a button is being pushed and any value above 36 means a button is not being pushed.  

## Report Button Pushes to the Screen

Next we will modify our code by adding additional statements to invoke the display and print button presses to the screen.  

Make sure you have imported the TFT_eSPI library and modified your User_Setup.h as detailed in the [screen text tutorial](Screen-Text-Tutorial.md)

Create additional constant integer variable to represent our button push threshold by adding this line below the existing constant declarations:

`const int min_touch_value = 20; //For this pcb any button push returns a value less than 20`

To use the display we will utilize the custom TFT_eSPI library, add these lines include these header files above the setup function:

`#include <TFT_eSPI.h>`  //add TFT_eSPI.h header file

Invoke the custom TFT_eSPI library by adding this line after the include statements (also placed above the setup function):

`TFT_eSPI tft = TFT_eSPI();`  //add instantiation of TFT_eSPI object 

Initalize the screen and print a startup message by adding code to the existing setup function to the screen by initializing the tft screen object as shown below

```
void setup() {
  // put your setup code here, to run once:
  tft.init();                               // add code to intialize the screen
  tft.setTextColor(TFT_WHITE, TFT_BLACK);   // add code to Set the font colour to be white with a black background
  tft.setCursor(0,0,2);                     // add code to set the cursor position to 0,0 with font 2
  tft.println("Button Test");               // add code to print button test message to the screen
  
  Serial.begin(115200);                     // start the serial monitor
  Serial.println("Button Test!");           // print button test messge to serial monitor
    
  delay(1000);                              // hold messages on the screen and serial monitor for 1 second
  tft.fillScreen(TFT_BLACK);                // add code to clear the screen by filling it with black
  
}

```

Add code to your loop function to detect a button push, utilizing the previously determined min_touch_value:

```

void loop() {
  // put your main code here, to run repeatedly:

  Serial.print("S0 touchRead value is ");
  Serial.println(touchRead( S0 ));  // get value of S0
  if (touchRead(S0) < min_touch_value) tft.println("S0!!");  //add code to compare S0 value to min touch value and display to screen if value is less
  
  Serial.print("S2 touchRead value is ");
  Serial.println(touchRead(S2));  // get value of S2
  if (touchRead(S2) < min_touch_value) tft.println("S2!!");   //add code to compare S2 value to min touch value and display to screen if value is less

  Serial.print("S3 touchRead value is ");
  Serial.println(touchRead(S3));  // get value of S3
  if (touchRead(S3) < min_touch_value) tft.println("S3!!");   //add code to compare S3 value to min touch value and display to screen if value is less

  Serial.print("S4 touchRead value is ");
  Serial.println(touchRead(S4));  // get value of S4
  if (touchRead(S4) < min_touch_value) tft.println("S4!!");  //add code to compare S4 value to min touch value and display to screen if value is less

  Serial.print("S5 touchRead value is ");
  Serial.println(touchRead(S5));  // get value of S5
  if (touchRead(S5) < min_touch_value) tft.println("S5!!");  //add code to compare S5 value to min touch value and display to screen if value is less

  Serial.print("S6 touchRead value is ");
  Serial.println(touchRead(S6));  // get value of S6
  if (touchRead(S6) < min_touch_value) tft.println("S6!!");  //add code to compare S6 value to min touch value and display to screen if value is less

  Serial.print("S7 touchRead value is ");
  Serial.println(touchRead(S7));  // get value of S7
  if (touchRead(S7) < min_touch_value) tft.println("S7!!");  //add code to compare S7 value to min touch value and display to screen if value is less

  Serial.print("S8 touchRead value is ");
  Serial.println(touchRead(S8));  // get value of S8
  if (touchRead(S8) < min_touch_value) tft.println("S8!!");  //add code to compare S2 value to min touch value and display to screen if value is less

  delay(500);  //sample values every 1/2 second (500 milliseconds)

  tft.fillScreen(TFT_BLACK);      // Clear the screen by filling it with black
  tft.setCursor(0,0,2);           // Reset the cursor position

  for (int i = 0; i <= 50; i++) {
    Serial.println(""); // clear a bunch of space to make it easier to read each sample in the monitor window
  }

}


```

Upload and run your code!!  Modify min_touch_value as necessary to ensure all buttons are functional.  


