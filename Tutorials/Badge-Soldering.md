(special thanks to Sudhir Desai)

<!-- TABLE OF CONTENTS -->
<details open="open">
<summary>Table of Contents</summary>
<ol>
<li>
<a href="https://cisco.box.com/s/4ejrhrkeo17py7m9tt7f20cd13x97568">Cleaning the Board</a>
</li>
<li>
<a href="https://cisco.box.com/s/bp9iwe1stf5a7o0r2pkfhgtrpmsjb0k2">Placing the
Daughterboard for USB Connectivity</a>
</li>
<li>
<a href="https://cisco.box.com/s/16qc3ife2k457z0xm0s5oci9ugpgbpio">A Quick Word About Soldering</a>
</li>
<li>
<a href="https://cisco.box.com/s/jl3qtja4k81l33db6ovwmxodw0hnuh7z">Soldering the USB
Daughterboard</a>
</li>
<li>
<a href="https://cisco.box.com/s/mv7fcrt8s59w5mv6tvxhqca0a2b5lfm0">Placing the Display</a>
</li>
<li>
<a href="https://cisco.box.com/s/wa99rgzibehiqj6s34he6lbr1fwkku8t">Soldering the Display</a>
</li>
<li>
<a href="https://cisco.box.com/s/yvk9n09mn757ikftygbhduftxbr9pljs">Confirming the Badge Works
on USB Power</a>
</li>
<li>
<a href="https://cisco.box.com/s/srq156kh8v66v1o0jm0y6gklvq1yogca">Inserting the Battery</a>
</li>
<li>
<a href="#FÍN">FÍN</a>
</li>
</ol>
</details>

<!-- Cleaning the Board -->

## Cleaning the Board

Whenever you receive or purchase a circuitboard, a good first step is to clean
all the manufacturing residue off the board with a circuit board cleaner.
<br />
For this purpose, I am using CRC QD Electronic Cleaner. I am calling this one
out by name because I have had good results using it in the past.  
(Click to play the video!!)<br />
[![000_png]][000_mp4]<br />

To use the cleaner, simply spray the board down with it and wipe it off.<br />
Please make sure that the board is dry before continuing with assembly.<br />
[![001_png]][001_mp4]<br />

<!-- Placing the Daughterboard for USB Connectivity -->

## Placing the Daughterboard for USB Connectivity

To mount the USB daughterboard to the badge, you will need the anti-static bag
with that component and the bag of standoffs labeled A (the set of 4 short
ones with 8 small phillips screws).<br />
My suggestion is to get everything physically located before assembly.<br />
[![002_png]][002_mp4]<br />

<!-- A Quick Word About Soldering -->

## A Quick Word About Soldering

The following soldered pins have too much solder applied. I have attempted to
wick some of the solder away using the solder wicking strip; however, I was
not as thorough as I could have been and left too much applied.<br />
On the other hand, the solder applied is shiny and not dull, so the pins
are acceptably fastened. To see what a more appropriate amount of solder is
for through-hole joints please skip down to
<a href="#soldering-the-display">Soldering the Display</a> and then come back
to this step.<br />
[![003_png]][003_mp4]<br />

<!-- Soldering the USB Daughterboard -->

## Soldering the USB Daughterboard

Soldering the USB daughterboard consists of two steps. The first step is to
solder the header onto the board. When soldering the header onto the board,
please make sure to support it from underneath as shown, so that the plastic
sits flush to the main circuitboard. As noted in the prior quick word, please
be sure not to over-apply solder.<br />
[![004_png]][004_mp4]<br />

After affixing the header pins, we can securely fasten the USB board to
the main circuitboard by way of the standoffs bundled in bag 'A’.<br />
When fastened securely, please solder the charging board to the main badge
board to ensure proper electrical connectivity.<br />
[![005_png]][005_mp4]<br />

<!-- Placing the Display -->

## Placing the Display

My strong preference is to make sure that the display sits as parallel as
possible to the badge circuitboard. To that end, I am using two of the nuts
from bag 'B’. When you get to this step you can do the same or use a piece
of conductive material to achieve the same goal. Please make sure that
you do use a conductive material since those pins are part of the ground
connection of the display.<br />
[![006_png]][006_mp4]<br />

<!-- Soldering the Display -->

## Soldering the Display

As mentioned above in <a href="#a-quick-word-about-soldering">A Quick Word
About Soldering</a>, please ensure that you use enough solder to connect the
components without using too much solder. The end of this clip illustrates
an appropriate amount of solder to use. If you notice the soldered joint
is dull instead of shiny, please dab or squirt some flux onto the dull joint
and reflow it. The flux will help pull the
contamination out of the joint and help ensure a high quality connection.<br />
[![007_png]][007_mp4]<br />

<!-- Confirming the Badge Works on USB Power -->

## Confirming the Badge Works on USB Power

After soldering the display, please use the USB-C cable to plug in the badge.
The display backlight should illuminate. If it does not, please ensure the
switch on the board next to the display is set to USB and try again.<br />
[![008_png]][008_mp4]<br />

<!-- Inserting the Battery -->

## Inserting the Battery

Once we have made sure the board works on USB power, please insert the battery
as shown. Please note that the positive (+) end of the battery has the plastic
jacket covering more of the lip, and the terminal diameter is slightly smaller.<br />
Also, you may encounter the situation I did where the terminals on the badge
were not bent out enough to make contact with the battery. Please do slightly
bend them out so that the battery’s electrical connection is secure.<br />
[![009_png]][009_mp4]<br />

<!-- FÍN -->

## FÍN

If you have made it this far, congratulations!<br />
Please don’t hesitate to speak up in the [Offensive Summit 2021][offsec_teams]
</a> WebEx Teams space with any questions, comments,
or concerns you may have.<br />

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[000_mp4]: https://cisco.box.com/s/s7rmlltcau6na7xpsr70o6vqg1owemgj
[000_png]: /Images/000_electronics-cleaner.png
[001_mp4]: https://cisco.box.com/s/4ejrhrkeo17py7m9tt7f20cd13x97568
[001_png]: /Images/001_clean-the-board.png
[002_mp4]: https://cisco.box.com/s/bp9iwe1stf5a7o0r2pkfhgtrpmsjb0k2
[002_png]: /Images/002_charging-board-and-orientation.png
[003_mp4]: https://cisco.box.com/s/0kssn339hbrzoplrgzp5qwx6mdscb448
[003_png]: /Images/003_a-quick-word-about-soldering.png
[004_mp4]: https://cisco.box.com/s/90qtwiemt0ge7sb3rxz1d99glkc0ie79
[004_png]: /Images/004_header-soldering-with-proper-support.png
[005_mp4]: https://cisco.box.com/s/jl3qtja4k81l33db6ovwmxodw0hnuh7z
[005_png]: /Images/005_mounting-the-charging-and-communications-port.png
[006_mp4]: https://cisco.box.com/s/mv7fcrt8s59w5mv6tvxhqca0a2b5lfm0
[006_png]: /Images/006_mounting-the-display-no-solder.png
[007_mp4]: https://cisco.box.com/s/wa99rgzibehiqj6s34he6lbr1fwkku8t
[007_png]: /Images/007_soldering-display.png
[008_mp4]: https://cisco.box.com/s/yvk9n09mn757ikftygbhduftxbr9pljs
[008_png]: /Images/008_confirming-wired-up.png
[009_mp4]: https://cisco.box.com/s/srq156kh8v66v1o0jm0y6gklvq1yogca
[009_png]: /Images/009_insert-battery-and-done.png
[offsec_teams]: https://eurl.io/#HkmRbajAx
